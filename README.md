Initial demo for exploration of polymer data

HOW TO BUILD:
1. Import into Unity and either run in editor or build with appropriate settings (Scene: "polymer-dec-scene")
2. Oculus SDK and Unity Integration have been updated several times recently.  This demo was built with the following versions:
	- Oculus SDK 0.5.0.1
	- Oculus Unity Integration 0.5.0.1
	- Unity 5.0.2f1 Personal 
3. On Windows, the demo has also been tested with the following Oculus SDK and Unity integration:
    - Oculus SDK 0.6.0.0
    - Oculus Unity Integration 0.6.0.0

============================================

USAGE:

The project should be opened in Unity, and
may be run from within Unity or built to run as a standalone application.

Interactions supported:
1. Standard Oculus interactions (head movements, including turning and moving towards or away from the head tracker)
2. Move forward, backward, left, or right using arrow keys (Alternate controls
may be mapped from the Input Manager within Unity).  The notion of "forward"
is determined by the gaze direction of the Oculus.
3. Decelerate by holding down the space bar (or map a different input button by adjusting 
the "Brake" axis in Unity).